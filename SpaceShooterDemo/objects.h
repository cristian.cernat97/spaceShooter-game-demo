//objects

enum IDS {PLAYER, BULLET, ENEMY};

//player
typedef struct Life{
	int x;
	int y;
	int val;
	int maxFrame;
	int curFrame;
	int frameCount;
	int frameDelay;
	int frameWidth;
	int frameHeight;
	int animationColumns;
	int animationDirection;

	int animationRow;
	ALLEGRO_BITMAP *image;
}Life;
typedef struct SpaceShip {
	int ID;
	int x;
	int y;
	int lives;
	int speed;
	int boundx;
	int boundy;
	int score;

	int maxFrame;
	int curFrame;
	int frameCount;
	int frameDelay;
	int frameWidth;
	int frameHeight;
	int animationColumns;
	int animationDirection;

	int animationRow;

	ALLEGRO_BITMAP *image;
} SpaceShip;

typedef struct Bullet {
	int ID;
	int x;
	int y;
	bool live;
	int speed;

	int boundx;
	int boundy;
	int maxFrame;
	int curFrame;
	int frameCount;
	int frameDelay;
	int frameWidth;
	int frameHeight;
	int animationColumns;
	int animationDirection;

	int animationRow;
	ALLEGRO_BITMAP *image;

}Bullet;

typedef struct Comet {
	int ID;
	int x;
	int y;
	bool live;
	int speed;
	int boundx;
	int boundy;

	int maxFrame;
	int curFrame;
	int frameCount;
	int frameDelay;
	int frameWidth;
	int frameHeight;
	int animationColumns;
	int animationDirection;

	ALLEGRO_BITMAP *image;
}Comet;

typedef struct Explosion {
	int x;
	int y;
	bool live;

	int maxFrame;
	int curFrame;
	int frameCount;
	int frameDelay;
	int frameWidth;
	int frameHeight;
	int animationColumns;
	int animationDirection;

	ALLEGRO_BITMAP *image;
}Explosion;

typedef struct Background {
	float x;
	float y;
	float velX;
	float velY;
	int dirX;
	int dirY;

	int width;
	int height;
	ALLEGRO_BITMAP *image;
}Background;

typedef struct UFO {
	int ID;
	int x;
	int y;
	bool live;
	int speed;
	int cavex;
	int cavey;
	int threshold;
	int boundx;
	int boundy;

	int maxFrame;
	int curFrame;
	int frameCount;
	int frameDelay;
	int frameWidth;
	int frameHeight;
	int animationColumns;
	int animationDirection;

	ALLEGRO_BITMAP *image;
	int *state;

}UFO;