#define _CRT_SECURE_NO_WARNINGS
#include <conio.h>
#include <stdio.h>
#include <math.h>
#include <allegro5\allegro.h>
#include <allegro5\allegro_primitives.h>
#include <allegro5\allegro_font.h>
#include <allegro5\allegro_ttf.h>
#include <allegro5\allegro_image.h>
#include <allegro5\allegro_audio.h>
#include <allegro5\allegro_acodec.h>
#include <time.h>
#include "objects.h"
#define NUM_BULLETS 5
#define NUM_COMETS 5
#define NUM_EXPLOSIONS 5
#define NUM_UFOS  2

//------------------------------
//---------GLOBALS--------------
//------------------------------
const int WIDTH = 800;
const int HEIGHT = 400;
bool checkWrite = false;
bool checkRead = false;
enum KEYS { UP, DOWN, LEFT, RIGHT, SPACE, ALT };
enum STATE { TITLE, PLAYING, LOST };
enum STATEUFO { IDLE, CHASING, RETREATING };
bool keys[6] = { false, false, false, false, false, false };

//------------------------------
//---------AUDIO----------------
//------------------------------
ALLEGRO_SAMPLE *shot = NULL;
ALLEGRO_SAMPLE *boom = NULL;
ALLEGRO_SAMPLE *song = NULL;

ALLEGRO_SAMPLE_INSTANCE *songInstance = NULL;

//------------------------------
//---------PROTOTYPES-----------
//------------------------------
void InitShip(SpaceShip *ship, ALLEGRO_BITMAP *image, ALLEGRO_DISPLAY *display);
void ResetShipAnimation(SpaceShip *ship, int position);
void DrawShip(SpaceShip *ship);
void MoveShipUp(SpaceShip *ship);
void MoveShipDown(SpaceShip *ship,ALLEGRO_DISPLAY *display);
void MoveShipLeft(SpaceShip *ship);
void MoveShipRight(SpaceShip *ship);

void InitBullet(Bullet bullet[], int size, ALLEGRO_BITMAP *image);
void DrawBullet(Bullet bullet[], int size);
void FireBullet(Bullet bullet[], int size, SpaceShip *ship);
void UpdateBullet(Bullet bullet[], int size, ALLEGRO_DISPLAY *display);
void CollideBullet(Bullet bullet[], int bSize, Comet comets[], int cSize, SpaceShip *ship, Explosion explosions[], int eSize, UFO ufos[], int uSize);

void InitComet(Comet comets[], int size, ALLEGRO_BITMAP *image);
void DrawComet(Comet comets[], int size);
void StartComet(Comet comets[], int size, ALLEGRO_DISPLAY *display);
void UpdateComet(Comet comets[], int size);
void CollideComet(Comet comets[], int cSize, SpaceShip *ship, Explosion explosions[], int eSize);

void InitExplosions(Explosion explosions[], int size, ALLEGRO_BITMAP *image);
void DrawExplosions(Explosion explosions[], int size);
void StartExplosions(Explosion explosions[], int size, int x, int y);
void UpdateExplosions(Explosion explosions[], int size);

void InitLife(Life *life, ALLEGRO_BITMAP *image);

void DrawLife(Life *life, SpaceShip* ship);

void ChangeState(int *state,
	int newState,
	SpaceShip *ship,
	Bullet bullets[],
	Comet comets[],
	Explosion explosions[],
	Life *life,
	UFO ufo[],
	ALLEGRO_BITMAP *shipImage,
	ALLEGRO_BITMAP *cometImage,
	ALLEGRO_BITMAP *explosionImage,
	ALLEGRO_BITMAP *bulletImage,
	ALLEGRO_BITMAP *lifeImage,
	ALLEGRO_BITMAP *ufoImage,
	ALLEGRO_DISPLAY *display);
void ChangeStateG(int *stateG, int newStateG);
void ChangeStateUFO(int *stateUFO, int newStateUFO);

float CheckDistance(int x1, int y1, int x2, int y2);
float AngleToTarget(int x1, int y1, int x2, int y2);
void InitUfo(UFO ufo[], int size, ALLEGRO_BITMAP *image);
void DrawUfo(UFO ufo[], int size);
void StartUfo(UFO ufo[], int size,ALLEGRO_DISPLAY *display);
void UpdateUfo(UFO ufo[], int size, int state, int *gState, SpaceShip *ship);
void CollideUfo(UFO ufo[], int size, SpaceShip *ship, Explosion explosions[], int eSize);

void InitBackground(Background *back, float x, float y, float velX, float velY, int width, int height, int dirX, int dirY, ALLEGRO_BITMAP *image);
void UpdateBackground(Background *back);
void DrawBackground(Background *back, ALLEGRO_DISPLAY *display);

//------------------------------
//---------MAIN-----------------
//------------------------------
int main(void)
{
	//------------------------------
	//---------PRIMITIVES-----------
	//------------------------------
	bool done = false;
	bool redraw = true;
	const int FPS = 60;

	int *state = malloc(sizeof(int));
	if (state == NULL)return -1;
	*state = -1;

	int *stateG;
	stateG = malloc(sizeof(int));
	*stateG = -1;

	FILE *fp = malloc(1024);
	if (fp == NULL)return -1;

	time_t rawtime;
	time(&rawtime);
	struct tm *timeinfo = malloc(sizeof(struct tm));
	if (timeinfo == NULL)return -1;
	timeinfo = localtime(&rawtime);

	int checkDisplay = 0;
	//------------------------------
	//---------OBJECT VARIABLES-----
	//------------------------------
	SpaceShip *ship = malloc(sizeof(SpaceShip));
	if (ship == NULL)return -1;

	Life *life = malloc(sizeof(Life));
	if (life == NULL)return -1;

	Bullet bullets[NUM_BULLETS];
	Comet comets[NUM_COMETS];

	Explosion explosions[NUM_EXPLOSIONS];
	UFO ufos[NUM_UFOS];
	Background *BG = malloc(sizeof(Background));
	Background *MG = malloc(sizeof(Background));
	Background *FG = malloc(sizeof(Background));
	if (BG == NULL)return -1;
	if (MG == NULL)return -1;
	if (FG == NULL)return -1;

	//------------------------------
	//---------ALLEGRO VARIABLES----
	//------------------------------
	ALLEGRO_DISPLAY *display = NULL;
	ALLEGRO_DISPLAY_MODE   disp_data;
	ALLEGRO_EVENT_QUEUE *event_queue = NULL;
	ALLEGRO_TIMER *timer = NULL;
	ALLEGRO_FONT *font18 = NULL;
	ALLEGRO_BITMAP *shipImage;
	ALLEGRO_BITMAP *cometImage;
	ALLEGRO_BITMAP *explosionImage;
	ALLEGRO_BITMAP *bulletImage = NULL;
	ALLEGRO_BITMAP *lifeImage = NULL;
	ALLEGRO_BITMAP *ufoImage = NULL;
	ALLEGRO_BITMAP *title = NULL;
	ALLEGRO_BITMAP *lost = NULL;
	ALLEGRO_BITMAP *bgImage = NULL;
	ALLEGRO_BITMAP *mgImage = NULL;
	ALLEGRO_BITMAP *fgImage = NULL;

	//------------------------------
	//---------INITIALIZATION-------
	//------------------------------
	if (!al_init())										//initialize Allegro
		return -1;

	display = al_create_display(WIDTH, HEIGHT);			//create our display object

	if (!display)										//test display object
		return -1;

	al_init_primitives_addon();
	al_install_keyboard();
	al_init_font_addon();
	al_init_ttf_addon();
	al_init_image_addon();
	al_install_audio();
	al_init_acodec_addon();

	event_queue = al_create_event_queue();
	timer = al_create_timer(1.0 / FPS);

	//------------------------------
	//---------ADDING-IMAGES--------
	//------------------------------
	shipImage = al_load_bitmap("rocket-flames-96.png");
	al_convert_mask_to_alpha(shipImage, al_map_rgb(255, 0, 255));

	cometImage = al_load_bitmap("meteor.png");
	explosionImage = al_load_bitmap("explosion2.png");
	bulletImage = al_load_bitmap("bullets2.png");
	lifeImage = al_load_bitmap("heart.png");
	ufoImage = al_load_bitmap("ufo.png");
	if (!cometImage)return -1;
	if (!bulletImage)return -1;
	if (!lifeImage)return -1;
	if (!explosionImage)return -1;
	if (!ufoImage)return -1;

	title = al_load_bitmap("enterGame.png");
	lost = al_load_bitmap("gameOver.png");


	bgImage = al_load_bitmap("main22BG.png");
	mgImage = al_load_bitmap("starMG.png");
	fgImage = al_load_bitmap("starFG.png");

	//------------------------------
	//---------SOUND-INIT-----------
	//------------------------------
	al_reserve_samples(10);//RESERVE 10 CHANNELS

	shot = al_load_sample("shot.ogg");
	boom = al_load_sample("boom.ogg");
	song = al_load_sample("song.ogg");
	if (!shot)return -1;
	if (!boom)return -1;
	if (!song)return -1;
	songInstance = al_create_sample_instance(song);
	al_set_sample_instance_playmode(songInstance, ALLEGRO_PLAYMODE_LOOP);
	al_attach_sample_instance_to_mixer(songInstance, al_get_default_mixer());//ATTACH TO MIXER

	 //------------------------------
	 //---------START-THE-GAME-------
	 //------------------------------
	srand(time(NULL));

	ChangeState(state, TITLE, ship, bullets, comets, explosions, life, ufos, shipImage, cometImage, explosionImage, bulletImage, lifeImage, ufoImage,display);

	//------------------------------
	//---------INIT-THE-GAME--------
	//------------------------------
	InitShip(ship, shipImage,display);
	InitBullet(bullets, NUM_BULLETS, bulletImage);
	InitComet(comets, NUM_COMETS, cometImage);
	InitExplosions(explosions, NUM_EXPLOSIONS, explosionImage);
	InitUfo(ufos, NUM_UFOS, ufoImage);
	InitLife(life, lifeImage);
	InitBackground(BG, 0, 0, 1, 0, 1280, 720, -1, 1, bgImage);
	InitBackground(MG, 0, 0, 3, 0, 1600, 600, -1, 1, mgImage);
	InitBackground(FG, 0, 0, 5, 0, 800, 600, -1, 1, fgImage);
	ChangeStateG(stateG, IDLE);
	//------------------------------
	//---------GET-THE-FONT---------
	//------------------------------
	font18 = al_load_font("arial.ttf", 18, 0);

	//------------------------------
	//---------REGISTER-EVENTS------
	//------------------------------
	al_register_event_source(event_queue, al_get_keyboard_event_source());
	al_register_event_source(event_queue, al_get_timer_event_source(timer));
	al_register_event_source(event_queue, al_get_display_event_source(display));

	al_start_timer(timer);
	while (!done)
	{
		ALLEGRO_EVENT ev;
		al_wait_for_event(event_queue, &ev);

		//--------------------------------
		//---------CHECK-TIMER------------
		//--------------------------------
		if (ev.type == ALLEGRO_EVENT_TIMER)
		{
			redraw = true;
			if (keys[UP])
				MoveShipUp(ship);
			else if (keys[DOWN])
				MoveShipDown(ship,display);
			else
				ResetShipAnimation(ship, 1);

			if (keys[LEFT])
				MoveShipLeft(ship);
			else if (keys[RIGHT])
				MoveShipRight(ship);
			else
				ResetShipAnimation(ship, 2);

			if (*state == TITLE) {

			}
			else if (*state == PLAYING) {
				//------------------------------
				//----UPDATE-THE-GAME-60FPS-----
				//------------------------------
				UpdateBackground(BG);
				UpdateBackground(MG);
				UpdateBackground(FG);
				UpdateExplosions(explosions, NUM_EXPLOSIONS);
				UpdateBullet(bullets, NUM_BULLETS, display);
				StartComet(comets, NUM_COMETS, display);
				UpdateComet(comets, NUM_COMETS);
				StartUfo(ufos, NUM_UFOS, display);
				if (*stateG == IDLE) {
					UpdateUfo(ufos, NUM_UFOS, CHASING, stateG, ship);
				}
				else if (*stateG == CHASING) {
					UpdateUfo(ufos, NUM_UFOS, RETREATING, stateG, ship);
				}
				else if (*stateG == RETREATING) {
					UpdateUfo(ufos, NUM_UFOS, IDLE, stateG, ship);//idle param will not work if changed
				}
				CollideBullet(bullets, NUM_BULLETS, comets, NUM_COMETS, ship, explosions, NUM_EXPLOSIONS, ufos, NUM_UFOS);
				CollideComet(comets, NUM_COMETS, ship, explosions, NUM_EXPLOSIONS);
				CollideUfo(ufos, NUM_UFOS, ship, explosions, NUM_EXPLOSIONS);

				if (ship->lives <= 0)
					ChangeState(state, LOST, ship, bullets, comets, explosions, life, ufos, shipImage, cometImage,
						explosionImage, bulletImage, lifeImage, ufoImage,display);
			}
			else if (*state == LOST) {
			}
		}
		else if (ev.type == ALLEGRO_EVENT_DISPLAY_CLOSE)
		{
			done = true;
		}

		//--------------------------------
		//---------KEY-PRESSED------------
		//--------------------------------
		else if (ev.type == ALLEGRO_EVENT_KEY_DOWN)
		{
			switch (ev.keyboard.keycode)
			{
			case ALLEGRO_KEY_ESCAPE:
				done = true;
				break;
			case ALLEGRO_KEY_UP:
				keys[UP] = true;
				break;
			case ALLEGRO_KEY_DOWN:
				keys[DOWN] = true;
				break;
			case ALLEGRO_KEY_LEFT:
				keys[LEFT] = true;
				break;
			case ALLEGRO_KEY_RIGHT:
				keys[RIGHT] = true;
				break;
			case ALLEGRO_KEY_ALT:
				keys[ALT] = true;
				if (*state == TITLE) {
					al_resize_display(display, WIDTH, HEIGHT);
					al_set_window_position(display, al_get_display_width(display), al_get_display_height(display));
				}
				else if (*state == PLAYING) {
					if (checkDisplay) {
						al_resize_display(display, WIDTH, HEIGHT);
						al_set_window_position(display, al_get_display_width(display) / 2, al_get_display_height(display) / 2);
						checkDisplay = 0;
					}
					else {
						al_acknowledge_resize(display);
						al_get_display_mode(al_get_num_display_modes() - 1, &disp_data);
						al_set_new_display_flags(ALLEGRO_FULLSCREEN);
						al_resize_display(display, disp_data.width, disp_data.height);
						al_set_window_position(display, 0, 0);
						checkDisplay = 1;
					}
				}
				else if (*state == LOST) {
					al_resize_display(display, WIDTH, HEIGHT);
					al_set_window_position(display, al_get_display_width(display) / 2, al_get_display_height(display) / 2);
					//plaay again when lost
				}
				break;
			case ALLEGRO_KEY_SPACE:
				keys[SPACE] = true;
				if (*state == TITLE)
					ChangeState(state, PLAYING, ship, bullets, comets, explosions, life, ufos,
						shipImage, cometImage, explosionImage, bulletImage, lifeImage, ufoImage,display);
				else if (*state == PLAYING)
					FireBullet(bullets, NUM_BULLETS, ship);

				else if (*state == LOST) {
					checkWrite = false;
					checkRead = false;
					ChangeState(state, PLAYING, ship, bullets, comets, explosions, life, ufos,
						shipImage, cometImage, explosionImage, bulletImage, lifeImage, ufoImage,display);
					//plaay again when lost
				}
				break;
			}
		}

		//--------------------------------
		//---------KEY-RELEASED-----------
		//--------------------------------
		else if (ev.type == ALLEGRO_EVENT_KEY_UP)
		{
			switch (ev.keyboard.keycode)
			{
			case ALLEGRO_KEY_ESCAPE:
				done = true;
				break;
			case ALLEGRO_KEY_UP:
				keys[UP] = false;
				break;
			case ALLEGRO_KEY_DOWN:
				keys[DOWN] = false;
				break;
			case ALLEGRO_KEY_LEFT:
				keys[LEFT] = false;
				break;
			case ALLEGRO_KEY_RIGHT:
				keys[RIGHT] = false;
				break;
			case ALLEGRO_KEY_SPACE:
				keys[SPACE] = false;
				break;
			case ALLEGRO_KEY_ALT:
				keys[ALT] = false;
				break;
			}
		}

		//------------------------------
		//---------DRAW-THE-GAME-------
		//------------------------------
		if (redraw && al_is_event_queue_empty(event_queue))
		{
			redraw = false;

			if (*state == TITLE) {
				al_draw_bitmap(title, 0, 0, 0);
			}
			else if (*state == PLAYING) {

				DrawBackground(BG, display);
				DrawBackground(MG, display);
				DrawBackground(FG, display);
				DrawShip(ship);
				DrawBullet(bullets, NUM_BULLETS);
				DrawComet(comets, NUM_COMETS);
				DrawUfo(ufos, NUM_UFOS);
				DrawExplosions(explosions, NUM_EXPLOSIONS);
				al_draw_textf(font18, al_map_rgb(255, 0, 255), 80, 5, 0, " Player has destroyed %i objects", ship->score);
				DrawLife(life, ship);

			}
			else if (*state == LOST) {
				if (!checkWrite) {
						al_resize_display(display, WIDTH, HEIGHT);
						al_set_window_position(display, al_get_display_width(display)/2, al_get_display_height(display)/2);

					fp = fopen("results.txt", "a");
					if (fp == NULL)
						exit(-1);
					fprintf(fp, "Score: %d at %d-%d-%d %d:%d:%d\n", ship->score, timeinfo->tm_year + 1900, timeinfo->tm_mon + 1, timeinfo->tm_mday, timeinfo->tm_hour, timeinfo->tm_min, timeinfo->tm_sec);
					fclose(fp);
					checkWrite = true;
				}

				al_draw_bitmap(lost, 0, 0, 0);
				al_draw_textf(font18, al_map_rgb(0, 255, 255), al_get_display_width(display) - 10, 10, ALLEGRO_ALIGN_RIGHT, " Final Score: %i", ship->score);

				//	if (!checkRead) {
				fp = fopen("results.txt", "rt");
				int downWrite = 30;
				int count = 0;
				long int pos = 0;
				char line[256];
				fseek(fp, 0, SEEK_END);
				pos = ftell(fp);
				/* Don't write each char on output.txt, just search for '\n' */
				while (pos) {
					fseek(fp, --pos, SEEK_SET); /* seek from begin */
					if (fgetc(fp) == '\n') {
						if (count++ == 10) break;
					}
				}
				while (fgets(line, sizeof(line), fp)) {
					strtok(line, "\n");
					al_draw_textf(font18, al_map_rgb(0, 255, 255), al_get_display_width(display) - 10, downWrite, ALLEGRO_ALIGN_RIGHT, " %s", line);
					downWrite += 30;
				}

				fclose(fp);
				//		checkRead = true;
					//}
			}


			al_flip_display();
			al_clear_to_color(al_map_rgb(0, 0, 0));
		}
	}
	//------------------------------
	//---------DESTROY-OBJECTS-------
	//------------------------------
	al_destroy_bitmap(bgImage);
	al_destroy_bitmap(mgImage);
	al_destroy_bitmap(fgImage);
	al_destroy_bitmap(title);
	al_destroy_bitmap(lost);
	al_destroy_bitmap(explosionImage);
	al_destroy_bitmap(cometImage);
	al_destroy_bitmap(shipImage);
	al_destroy_bitmap(ufoImage);
	al_destroy_event_queue(event_queue);
	al_destroy_timer(timer);
	al_destroy_font(font18);
	al_destroy_display(display);						//destroy our display object

	return 0;
}

//------------------------------
//---------METHODS--------------
//------------------------------

//------------------------------
//---------SHIP-----------------
//------------------------------
void InitShip(SpaceShip *ship, ALLEGRO_BITMAP *image, ALLEGRO_DISPLAY *display) {

	ship->x = 20;
	ship->y = al_get_display_height(display) / 2;
	ship->ID = PLAYER;
	ship->lives = 3;
	ship->speed = 6;
	ship->boundx = 80;
	ship->boundy = 50;
	ship->score = 0;

	ship->maxFrame = 2;
	ship->curFrame = 0;
	ship->frameCount = 0;
	ship->frameDelay = 50;
	ship->frameWidth = 156; //length sheet spirte
	ship->frameHeight = 96;
	ship->animationColumns = 2;
	ship->animationDirection = 1;

	ship->animationRow = 0;

	ship->image = image;
}
void ResetShipAnimation(SpaceShip *ship, int position)
{
	if (position == 1)
		ship->animationRow = 1;
	else
		ship->curFrame = 0;
}
void DrawShip(SpaceShip *ship)
{
	int fx = (ship->curFrame % ship->animationColumns) * ship->frameWidth;
	int fy = ship->animationRow * ship->frameHeight;

	al_draw_bitmap_region(ship->image, fx, fy, ship->frameWidth,
		ship->frameHeight, ship->x - ship->frameWidth / 2, ship->y - ship->frameHeight / 2, 0);

	//al_draw_filled_rectangle(ship->x - ship->boundx, ship->y - ship->boundy, ship->x + ship->boundx,
	//	ship->y + ship->boundy, al_map_rgba(255, 0, 255, 100));
}
void MoveShipUp(SpaceShip *ship)
{
	ship->animationRow = 0;
	ship->y -= ship->speed;
	if (ship->y < 0)
		ship->y = 0;
}
void MoveShipDown(SpaceShip *ship,ALLEGRO_DISPLAY *display)
{
	ship->animationRow = 2;
	ship->y += ship->speed;
	if (ship->y > al_get_display_height(display))
		ship->y = al_get_display_height(display);
}
void MoveShipLeft(SpaceShip *ship)
{
	ship->curFrame = 1;
	ship->x -= ship->speed;
	if (ship->x < 0)
		ship->x = 0;
}
void MoveShipRight(SpaceShip *ship)
{
	ship->curFrame = 2;
	ship->x += ship->speed;
	if (ship->x > 600)
		ship->x = 600;
}

//--------------------------------
//---------BULLET-----------------
//--------------------------------
void InitBullet(Bullet bullet[], int size, ALLEGRO_BITMAP *image)
{
	for (int i = 0; i < size; i++)
	{
		bullet[i].ID = BULLET;
		bullet[i].live = false;
		bullet[i].speed = 10;
		bullet[i].boundx = 40;
		bullet[i].boundy = 40;

		bullet[i].maxFrame = 7;//everty time half of frame width or height
		bullet[i].curFrame = 0;
		bullet[i].frameCount = 0;
		bullet[i].frameDelay = 6;
		bullet[i].frameWidth = 77;
		bullet[i].frameHeight = 49;
		bullet[i].animationColumns = 7;

		if (rand() % 2)
			bullet[i].animationDirection = 1;
		else bullet[i].animationDirection = -1;

		bullet[i].image = image;
	}
}
void DrawBullet(Bullet bullet[], int size)
{
	for (int i = 0; i < size; i++) {
		if (bullet[i].live) {
			al_draw_filled_circle(bullet[i].x, bullet[i].y, 2, al_map_rgb(255, 255, 255));
			int fx = (bullet[i].curFrame % bullet[i].animationColumns) * bullet[i].frameWidth;
			int fy = (bullet[i].curFrame / bullet[i].animationColumns) * bullet[i].frameHeight;

			al_draw_bitmap_region(bullet[i].image, fx, fy, bullet[i].frameWidth,
				bullet[i].frameHeight, bullet[i].x - bullet[i].frameWidth / 2, bullet[i].y - bullet[i].frameHeight / 2, 0);


			//al_draw_filled_rectangle(bullet[i].x - bullet[i].boundx, bullet[i].y - bullet[i].boundy, bullet[i].x + bullet[i].boundx,
			//	bullet[i].y + bullet[i].boundy,al_map_rgba(255,0,255,100));
		}

	}
}
void FireBullet(Bullet bullet[], int size, SpaceShip *ship)
{
	for (int i = 0; i < size; i++)
	{
		if (!bullet[i].live)
		{
			bullet[i].x = ship->x + 17;
			bullet[i].y = ship->y + 10;
			bullet[i].live = true;
			al_play_sample(shot, 1, 0, 1, ALLEGRO_PLAYMODE_ONCE, 0);
			break;
		}
	}
}
void UpdateBullet(Bullet bullet[], int size,ALLEGRO_DISPLAY *display)
{
	for (int i = 0; i < size; i++)
	{
		if (bullet[i].live)
		{

			if (++bullet[i].frameCount >= bullet[i].frameDelay)
			{
				bullet[i].curFrame += bullet[i].animationDirection;
				if (bullet[i].curFrame >= bullet[i].maxFrame)
					bullet[i].curFrame = 0;
				else if (bullet[i].curFrame <= 0)
					bullet[i].curFrame = bullet[i].maxFrame - 1;

				bullet[i].frameCount = 0;
			}

			bullet[i].x += bullet[i].speed;
			if (bullet[i].x > al_get_display_width(display))
				bullet[i].live = false;
		}
	}
}
void CollideBullet(Bullet bullet[], int bSize, Comet comets[], int cSize, SpaceShip *ship, Explosion explosions[], int eSize, UFO ufos[], int uSize)
{
	for (int i = 0; i < bSize; i++)
	{
		if (bullet[i].live)
		{
			for (int j = 0; j < cSize; j++)
			{
				if (comets[j].live)
				{
					if (bullet[i].x > (comets[j].x - comets[j].boundx) &&
						bullet[i].x < (comets[j].x + comets[j].boundx) &&
						bullet[i].y >(comets[j].y - comets[j].boundy) &&
						bullet[i].y < (comets[j].y + comets[j].boundy))
					{
						bullet[i].live = false;
						comets[j].live = false;

						ship->score++;

						StartExplosions(explosions, eSize, bullet[i].x, bullet[i].y);

						al_play_sample(boom, 1, 0, 1, ALLEGRO_PLAYMODE_ONCE, 0);
					}
				}
			}
			for (int j = 0; j < uSize; j++)
			{
				if (ufos[j].live)
				{
					if (bullet[i].x > (ufos[j].x - ufos[j].boundx) &&
						bullet[i].x < (ufos[j].x + ufos[j].boundx) &&
						bullet[i].y >(ufos[j].y - ufos[j].boundy) &&
						bullet[i].y < (ufos[j].y + ufos[j].boundy))
					{
						bullet[i].live = false;
						ufos[j].live = false;

						ship->score++;

						StartExplosions(explosions, eSize, bullet[i].x, bullet[i].y);

						al_play_sample(boom, 1, 0, 1, ALLEGRO_PLAYMODE_ONCE, 0);
					}
				}
			}
		}
	}
}

//--------------------------------
//---------COMET-----------------
//--------------------------------
void InitComet(Comet comets[], int size, ALLEGRO_BITMAP *image)
{
	for (int i = 0; i < size; i++)
	{
		comets[i].ID = ENEMY;
		comets[i].live = false;
		comets[i].speed = 5;
		comets[i].boundx = 18;
		comets[i].boundy = 18;

		comets[i].maxFrame = 30;//everty time half of frame width or height
		comets[i].curFrame = 0;
		comets[i].frameCount = 0;
		comets[i].frameDelay = 2;
		comets[i].frameWidth = 64;
		comets[i].frameHeight = 64;
		comets[i].animationColumns = 5;


		if (rand() % 2)
			comets[i].animationDirection = 1;
		else comets[i].animationDirection = -1;

		comets[i].image = image;

	}
}
void DrawComet(Comet comets[], int size)
{
	for (int i = 0; i < size; i++)
	{
		if (comets[i].live)
		{
			int fx = (comets[i].curFrame % comets[i].animationColumns) * comets[i].frameWidth;
			int fy = (comets[i].curFrame / comets[i].animationColumns) * comets[i].frameHeight;

			al_draw_bitmap_region(comets[i].image, fx, fy, comets[i].frameWidth,
				comets[i].frameHeight, comets[i].x - comets[i].frameWidth / 2, comets[i].y - comets[i].frameHeight / 2, 0);
			//al_draw_filled_rectangle(comets[i].x - comets[i].boundx, comets[i].y - comets[i].boundy, comets[i].x + comets[i].boundx,
			//	comets[i].y + comets[i].boundy, al_map_rgba(255, 0, 255, 100));
		}
	}
}
void StartComet(Comet comets[], int size, ALLEGRO_DISPLAY *display)
{
	for (int i = 0; i < size; i++)
	{
		if (!comets[i].live)
		{
			if (rand() % 500 == 0)
			{
				comets[i].live = true;
				comets[i].x = al_get_display_width(display);
				comets[i].y = 30 + rand() % (al_get_display_height(display) - 60);

				break;
			}
		}
	}
}
void UpdateComet(Comet comets[], int size)
{
	for (int i = 0; i < size; i++)
	{
		if (comets[i].live)
		{
			if (++comets[i].frameCount >= comets[i].frameDelay)
			{
				comets[i].curFrame += comets[i].animationDirection;
				if (comets[i].curFrame >= comets[i].maxFrame)
					comets[i].curFrame = 0;
				else if (comets[i].curFrame <= 0)
					comets[i].curFrame = comets[i].maxFrame - 1;

				comets[i].frameCount = 0;
			}

			comets[i].x -= comets[i].speed;
		}
	}
}
void CollideComet(Comet comets[], int cSize, SpaceShip *ship, Explosion explosions[], int eSize)
{
	for (int i = 0; i < cSize; i++)
	{
		if (comets[i].live)
		{
			if (comets[i].x - comets[i].boundx < ship->x + ship->boundx &&
				comets[i].x + comets[i].boundx > ship->x - ship->boundx &&
				comets[i].y - comets[i].boundy < ship->y + ship->boundy &&
				comets[i].y + comets[i].boundy > ship->y - ship->boundy)
			{
				ship->lives--;
				comets[i].live = false;
				StartExplosions(explosions, eSize, ship->x, ship->y);
				al_play_sample(boom, 1, 0, 1, ALLEGRO_PLAYMODE_ONCE, 0);
			}
			else if (comets[i].x < 0)
			{
				comets[i].live = false;
				ship->lives--;
			}
		}
	}
}

//--------------------------------
//---------EXPLOSION-----------------
//--------------------------------
void InitExplosions(Explosion explosions[], int size, ALLEGRO_BITMAP *image) {
	for (int i = 0; i < size; i++)
	{
		explosions[i].live = false;
		explosions[i].maxFrame = 3;
		explosions[i].curFrame = 0;
		explosions[i].frameCount = 0;
		explosions[i].frameDelay = 2;
		explosions[i].frameWidth = 128;
		explosions[i].frameHeight = 128;
		explosions[i].animationColumns = 4;
		explosions[i].animationDirection = 1;

		explosions[i].image = image;
	}
}
void DrawExplosions(Explosion explosions[], int size) {
	for (int i = 0; i < size; i++)
	{
		if (explosions[i].live) {
			int fx = (explosions[i].curFrame % explosions[i].animationColumns) * explosions[i].frameWidth;
			int fy = (explosions[i].curFrame / explosions[i].animationColumns) * explosions[i].frameHeight;
			al_draw_bitmap_region(explosions[i].image, fx, fy, explosions[i].frameWidth,
				explosions[i].frameHeight, explosions[i].x - explosions[i].frameWidth / 2, explosions[i].y - explosions[i].frameHeight / 2, 0);
		}
	}

}
void StartExplosions(Explosion explosions[], int size, int x, int y) {
	for (int i = 0; i < size; i++) {
		if (!explosions[i].live) {
			explosions[i].live = true;
			explosions[i].x = x;
			explosions[i].y = y;
			break;
		}
	}
}
void UpdateExplosions(Explosion explosions[], int size) {
	for (int i = 0; i < size; i++)
	{
		if (explosions[i].live) {
			if (++explosions[i].frameCount >= explosions[i].frameDelay) {
				explosions[i].curFrame += explosions[i].animationDirection;
				if (explosions[i].curFrame >= explosions[i].maxFrame) {
					explosions[i].curFrame = 0;
					explosions[i].live = false;
				}
				explosions[i].frameCount = 0;
			}
		}
	}
}

//--------------------------------
//---------STATE------------------
//--------------------------------
void ChangeState(
	int *state,
	int newState,
	SpaceShip *ship,
	Bullet bullets[],
	Comet comets[],
	Explosion explosions[],
	Life *life,
	UFO ufo[],
	ALLEGRO_BITMAP *shipImage,
	ALLEGRO_BITMAP *cometImage,
	ALLEGRO_BITMAP *explosionImage,
	ALLEGRO_BITMAP *bulletImage,
	ALLEGRO_BITMAP *lifeImage,
	ALLEGRO_BITMAP *ufoImage,
	ALLEGRO_DISPLAY *display) {
	if (*state == TITLE) {

	}
	else if (*state == PLAYING) {
		al_stop_sample_instance(songInstance);
	}
	else if (*state == LOST) {

	}

	*state = newState;

	if (*state == TITLE) {

	}
	else if (*state == PLAYING) {
		InitShip(ship, shipImage, display);
		InitBullet(bullets, NUM_BULLETS, bulletImage);
		InitComet(comets, NUM_COMETS, cometImage);
		InitExplosions(explosions, NUM_EXPLOSIONS, explosionImage);
		InitLife(life, lifeImage);
		InitUfo(ufo, NUM_UFOS, ufoImage);
		al_play_sample_instance(songInstance);

	}
	else if (*state == LOST) {

	}
}

//--------------------------------
//---------BACKGROUND-----------------
//--------------------------------
void InitBackground(Background *back, float x, float y, float velX, float velY, int width, int height, int dirX, int dirY, ALLEGRO_BITMAP *image) {
	back->x = x;
	back->y = y;
	back->velX = velX;
	back->velY = velY;
	back->width = width;
	back->height = height;
	back->dirX = dirX;
	back->dirY = dirY;
	back->image = image;
}
void UpdateBackground(Background *back) {
	back->x += back->velX*back->dirX;
	if (back->x + back->width <= 0)
		back->x = 0;
}
void DrawBackground(Background *back, ALLEGRO_DISPLAY *display) {
	al_draw_bitmap(back->image, back->x, back->y, 0);
	if (back->x + back->width < al_get_display_width(display)) {
		al_draw_bitmap(back->image, back->x + back->width, back->y, 0);
	}
}

//--------------------------------
//---------LIFE-----------------
//--------------------------------
void InitLife(Life *life, ALLEGRO_BITMAP *image) {
	life->x = 30;
	life->y = 20;
	life->val = 3;
	life->maxFrame = 3;
	life->curFrame = 0;
	life->frameWidth = 30; //length sheet spirte
	life->frameHeight = 30;
	life->animationColumns = 3;
	life->animationDirection = 1;

	life->animationRow = 0;
	life->image = image;
}
void DrawLife(Life *life, SpaceShip *ship) {
	if (life->curFrame > 4)life->curFrame = 3;
	int fx = life->curFrame * life->frameWidth;
	int fy = life->frameHeight;

	if (life->val > ship->lives) {
		life->curFrame++;
		life->val = ship->lives;
	}
	al_draw_bitmap_region(life->image, fx, 0, life->frameWidth,
		life->frameHeight, 20, 5, 0);
}

//--------------------------------
//---------UFO--------------------
//--------------------------------
void ChangeStateUFO(int *stateUFO, int newStateUFO)
{

	if (*stateUFO == IDLE)
	{
		printf("Leaving the IDLE state UFO\n");
	}
	else if (*stateUFO == CHASING)
	{
		printf("Leaving the CHASING state UFO\n");
	}
	else if (*stateUFO == RETREATING)
	{
		printf("Leaving the RETREATING state UFO\n");
	}

	*stateUFO = newStateUFO;

	if (*stateUFO == IDLE)
	{
		printf("Now IDLING UFO\n");
	}
	else if (*stateUFO == CHASING)
	{
		printf("Now CHASING the player UFO\n");
	}
	else if (*stateUFO == RETREATING)
	{
		printf("Now RETREATING back to my cave UFO\n");
	}
}

void ChangeStateG(int *stateG, int newStateG)
{
	if (*stateG == IDLE)
	{
		printf("Leaving the IDLE state\n");
	}
	else if (*stateG == CHASING)
	{
		printf("Leaving the CHASING state\n");
	}
	else if (*stateG == RETREATING)
	{
		printf("Leaving the RETREATING state\n");
	}

	*stateG = newStateG;

	if (*stateG == IDLE)
	{
		printf("Now IDLING\n");
	}
	else if (*stateG == CHASING)
	{
		printf("Now CHASING the player\n");
	}
	else if (*stateG == RETREATING)
	{
		printf("Now RETREATING back to my cave\n");
	}
}

float CheckDistance(int x1, int y1, int x2, int y2)
{
	return sqrt(pow((float)x1 - x2, 2) + pow((float)y1 - y2, 2));
}

float AngleToTarget(int x1, int y1, int x2, int y2)
{
	float deltaX = (x2 - x1);
	float deltaY = (y2 - y1);
	return atan2(deltaY, deltaX);
}
void InitUfo(UFO ufo[], int size, ALLEGRO_BITMAP *image) {

	for (int i = 0; i < size; i++) {
		ufo[i].ID = ENEMY;
		ufo[i].live = false;
		ufo[i].speed = 2;
		ufo[i].threshold = 200;
		ufo[i].boundx = 30;
		ufo[i].boundy = 30;
		ufo[i].state = malloc(sizeof(int));
		*(ufo[i].state) = IDLE;

		ufo[i].maxFrame = 30;//everty time half of frame width or height
		ufo[i].curFrame = 0;
		ufo[i].frameCount = 0;
		ufo[i].frameDelay = 4;
		ufo[i].frameWidth = 64;
		ufo[i].frameHeight = 64;
		ufo[i].animationColumns = 9;

		if (rand() % 2)
			ufo[i].animationDirection = 1;
		else ufo[i].animationDirection = -1;

		ufo[i].image = image;
	}

}
void StartUfo(UFO ufo[], int size, ALLEGRO_DISPLAY *display)
{
	for (int i = 0; i < size; i++)
	{
		if (!ufo[i].live)
		{
			if (rand() % 500 == 0)
			{
				ufo[i].live = true;
				ufo[i].x = al_get_display_width(display);
				ufo[i].y = 30 + rand() % (al_get_display_height(display) - 60);
				ufo[i].cavex = ufo[i].x;
				ufo[i].cavey = ufo[i].y;
				break;
			}
		}
	}
}
void UpdateUfo(UFO ufo[], int size, int newState, int *gState, SpaceShip *ship) {
	for (int i = 0; i < size; i++) {

		if (ufo[i].live) {

			if (*(ufo[i].state) == IDLE)
				if (ufo[i].threshold > CheckDistance(ship->x, ship->y, ufo[i].x, ufo[i].y)) {
					ChangeStateUFO(ufo[i].state, newState);
					ChangeStateG(gState, newState);
				}
			if (*(ufo[i].state) == CHASING)
				if (ufo[i].threshold < CheckDistance(ufo[i].x, ufo[i].y, ufo[i].cavex, ufo[i].cavey)) {
					ChangeStateUFO(ufo[i].state, newState);
					ChangeStateG(gState, newState);
				}
				else
				{
					float angle = AngleToTarget(ufo[i].x, ufo[i].y, ship->x, ship->y);
					ufo[i].y += (2 * sin(angle));
					ufo[i].x += (2 * cos(angle));
					if (ufo[i].threshold < CheckDistance(ufo[i].x, ufo[i].y, ship->x, ship->y)) {
						ChangeStateUFO(ufo[i].state, newState);
						ChangeStateG(gState, newState);
					}
				}
			if (*(ufo[i].state) == RETREATING)
				if (5 >= CheckDistance(ufo[i].x, ufo[i].y, ufo[i].cavex, ufo[i].cavey)) {
					ufo[i].x = ufo[i].cavex;
					ufo[i].y = ufo[i].cavey;
					ChangeStateUFO(ufo[i].state, IDLE);
					ChangeStateG(gState, newState);
				}
				else
				{
					float angle = AngleToTarget(ufo[i].x, ufo[i].y, ufo[i].cavex, ufo[i].cavey);
					ufo[i].y += (3 * sin(angle));
					ufo[i].x += (3 * cos(angle));
					if (ufo[i].threshold > CheckDistance(ufo[i].x, ufo[i].y, ship->boundx, ship->boundy)) {
						ChangeStateUFO(ufo[i].state, CHASING);
						ChangeStateG(gState, newState);
					}

				}

			if (++ufo[i].frameCount >= ufo[i].frameDelay)
			{
				ufo[i].curFrame += ufo[i].animationDirection;
				if (ufo[i].curFrame >= ufo[i].maxFrame)
					ufo[i].curFrame = 0;
				else if (ufo[i].curFrame <= 0)
					ufo[i].curFrame = ufo[i].maxFrame - 1;

				ufo[i].frameCount = 0;
			}
			ufo[i].x -= ufo[i].speed;
			ufo[i].cavex -= ufo[i].speed;
		}
	}
}
void DrawUfo(UFO ufo[], int size) {

	for (int i = 0; i < size; i++) {
		if (ufo[i].live) {
			int fx = (ufo[i].curFrame % ufo[i].animationColumns) * ufo[i].frameWidth;
			int fy = (ufo[i].curFrame / ufo[i].animationColumns) * ufo[i].frameHeight;

			al_draw_bitmap_region(ufo[i].image, fx, fy, ufo[i].frameWidth,
				ufo[i].frameHeight, ufo[i].x - ufo[i].frameWidth / 2, ufo[i].y - ufo[i].frameHeight / 2, 0);

			//al_draw_circle(ufo[i].cavex, ufo[i].cavey, ufo[i].threshold, al_map_rgba_f(.5, .5, .5, .5), 1);
			//al_draw_circle(ufo[i].x, ufo[i].y, ufo[i].threshold, al_map_rgba_f(.5, 0, .5, .5), 1);

			//al_draw_filled_rectangle(ufo[i].x - ufo[i].boundx, ufo[i].y - ufo[i].boundy, ufo[i].x + ufo[i].boundx,
				//ufo[i].y + ufo[i].boundy, al_map_rgba(255, 0, 255, 100));
		}
	}
}
void CollideUfo(UFO ufo[], int size, SpaceShip *ship, Explosion explosions[], int eSize) {
	for (int i = 0; i < size; i++)
	{
		if (ufo[i].live)
		{
			if (ufo[i].x - ufo[i].boundx < ship->x + ship->boundx &&
				ufo[i].x + ufo[i].boundx > ship->x - ship->boundx &&
				ufo[i].y - ufo[i].boundy < ship->y + ship->boundy &&
				ufo[i].y + ufo[i].boundy > ship->y - ship->boundy)
			{
				ship->lives--;
				ufo[i].live = false;
				StartExplosions(explosions, eSize, ship->x, ship->y);
				al_play_sample(boom, 1, 0, 1, ALLEGRO_PLAYMODE_ONCE, 0);
			}
			else if (ufo[i].x < 0)
			{
				ufo[i].live = false;
				ship->lives--;
			}
		}
	}
}
