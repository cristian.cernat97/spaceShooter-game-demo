# Project Title

The First Spaceshooter

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes. See deployment for notes on how to deploy the project on a live system.


### Prerequisites

What things you need to install the software and how to install them

```
https://www.visualstudio.com/downloads/
```

### Installing

A step by step series of examples that tell you have to get a development env running

Say what the step will be

```
git clone https://gitlab.com/cristian.cernat97/spaceShooter-game-demo.git
```

End with an example of getting some data out of the system or using it for a little demo

## Running the tests

Installer does not work because of ”CRT WARNINGS”

## Gameplay
*Press ```Alt``` for fullscreen
*Press ``` the Arrows``` for movement
*Press ```Spacebar``` for a new game
## Deployment

No such thing now

## Built With

* [Allegro5](http://liballeg.org) - C library
* [Sprites](google them) - Do no own any of sprites
* [Music](google them) - Do now any of them

## Contributing

Go ahead blame me for anything I am still learning 
## Versioning

Version 1.0 

## Authors

* **Cristian Cernat** - (https://gitlab.com/cristian.cernat97)

## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details